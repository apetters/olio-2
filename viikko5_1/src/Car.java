
import java.lang.reflect.Array;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */


public class Car {
    private ArrayList<Object> parts;
    
    Car() {
        //System.out.println("Nyt tehdään auto");
        parts = new ArrayList<>();
        parts.add (new Body());
        parts.add (new Chassis());
        parts.add (new Engine());
        ArrayList<Wheel> wheels = new ArrayList<>();
        for (int i = 0; i<4; i++){
            wheels.add(new Wheel());
        }
        parts.add(wheels);
    }
    
    public void print(){
        System.out.println("Autoon kuuluu:");
            Class Classes[] = this.getClass().getDeclaredClasses();
        for (Object part : this.parts) {
            if (part instanceof ArrayList){
                System.out.println("\t"+ ((ArrayList)part).size() + " "+((ArrayList)part).get(0).getClass().getSimpleName());
            }
            else{
            System.out.println("\t" + part.getClass().getSimpleName());
            }
        }
    }
    
    private class Body {
        Body(){
            System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
        }
    }
    
    private class Engine {
        Engine(){
            System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
        }
        
    }
    
    private class Wheel {
        Wheel(){
            System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
        }
    
    }

    private class Chassis {
        Chassis() {
            System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
        }
    
    }

}