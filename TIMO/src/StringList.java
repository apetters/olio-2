
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class StringList extends ArrayList<String>{
    public StringList(){
        super();
    }
    
    public boolean containsString(String s){
        for (String t: this)
            if (t.equals(s))
                return true;
        return false;
    }
    
}
