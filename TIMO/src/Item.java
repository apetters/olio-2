
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class Item {
    private boolean fragile, broken;
    private double width, height, length, weight; //SI units, i.e. weight in Kg and sizes in m
    private String name, description;
    
    public Item (String n, String d, boolean f, double x, double y, double z, double w) {
        name = n;
        description = d;
        fragile = f;
        width = x;
        length = y;
        height = z;
        weight = w;
        broken = false;
    }
    //clone constructor 
    public Item (Item i) {
        name = i.name;
        description = i.description;
        fragile = i.fragile;
        width = i.width;
        length = i.length;
        height = i.height;
        weight = i.weight;
        //return new Item (name,description,fragile,width,length,height,weight);
    }
    
    public void bump(){
        if (fragile)
            broken = true;
        System.out.println("bump!");
    }
    
    public String getName(){
        return name;
    }
    
    public boolean getBroken(){
        return broken;
    }
    
    @Override
    
    public String toString(){
        return name;
    }
    
    public String getDescription(){
        DecimalFormat df = new DecimalFormat("####.###");//mm and g precision
        return description + ". dimensions: "+df.format(width)+" m x "+df.format(length)+ " m x " 
                + df.format(height) + " m, weight: " + df.format(weight) + "kg" + (fragile? ". Särkyvä" : "");
    }
    
    public ArrayList<Double> getDimensions(){
        //returns a vector containing x, y, z, cordinates and weight in that order
        ArrayList<Double> retval = new ArrayList<>();
        retval.add(length);
        retval.add(width);
        retval.add(height);
        retval.add(weight);
        return retval;
    }
}
