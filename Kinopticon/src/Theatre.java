/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Theatre {
    private int id;
    private String name;
    
    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    
    public Theatre(int i, String n){
        id = i;
        name = n;
    }
    @Override
    public String toString(){
        return name;
    }
}
