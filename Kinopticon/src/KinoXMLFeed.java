
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class KinoXMLFeed {
    
    public KinoXMLFeed() {
    //todo.. or rather nothin to do?
}
    
    public ArrayList<Theatre> getTheatres(){
        ArrayList<Theatre> retlist= new ArrayList();
        Document docu;
        URL finnkino;
        String output="", buffer;
        try {
             finnkino = new URL ("http://www.finnkino.fi/xml/TheatreAreas/");
             DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
             DocumentBuilder db = dbf.newDocumentBuilder();
             BufferedReader br = new BufferedReader(new InputStreamReader(finnkino.openStream()));
             while ((buffer=br.readLine())!=null)
                 output += buffer + "\n";
             docu = db.parse(new InputSource(new StringReader(output)));
             docu.getDocumentElement().normalize();
             //output = docu.getXmlVersion();
             //output="";
             NodeList nodes = docu.getElementsByTagName("TheatreArea");
             for (int i=0; i< nodes.getLength(); i++){
                 Element e = (Element)nodes.item(i);
                 String name = e.getElementsByTagName("Name").item(0).getTextContent();
                 int id = new Integer (e.getElementsByTagName("ID").item(0).getTextContent());
                 retlist.add(new Theatre (id, name));
                 //output += "name: "+ name + " ID: " + id +"\n"
                 }
             //outputField.setText(output);
        
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL");
        } catch (IOException ex){
            System.out.println("IOException");
        } catch (SAXException ex) {
            System.out.println("SAXException");
        } catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException");
        }
        retlist.remove(0); //tämä on "valitse alue"-elementti
        return retlist;
    }
    
    public ArrayList<Movie> getMovies(int id, String date){
        String source = "http://www.finnkino.fi/xml/Schedule/?area=" +id+ "&dt="+ date;
        Document docu;
        URL finnkino;
        String output="", buffer;
        ArrayList<Movie> retval = new ArrayList();
        System.out.println("Entering getMovies");
        try {
             finnkino = new URL (source);
             DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
             DocumentBuilder db = dbf.newDocumentBuilder();
             BufferedReader br = new BufferedReader(new InputStreamReader(finnkino.openStream()));
             while ((buffer=br.readLine())!=null)
                 output += buffer + "\n";
             docu = db.parse(new InputSource(new StringReader(output)));
             docu.getDocumentElement().normalize();
             //output = docu.getXmlVersion();
             output="";
             NodeList nodes = docu.getElementsByTagName("Show");
             for (int i=0; i< nodes.getLength(); i++){
                 Element e = (Element)nodes.item(i);
                 String title = e.getElementsByTagName("Title").item(0).getTextContent();
                 String datestring = e.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                 Date starts = null;
                 try{
                    starts = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(datestring);
                 } catch (ParseException ex){
                     System.out.println("Problem parsing starting time");
                 }
                 //int id = new Integer (e.getElementsByTagName("ID").item(0).getTextContent());
                 //retlist.add(new Theatre (id, name));
                 retval.add(new Movie(title, starts));
                 output += title + "\n";
                 }
             //outputField.setText(output);
        
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL");
        } catch (IOException ex){
            System.out.println("IOException");
        } catch (SAXException ex) {
            System.out.println("SAXException");
        } catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException");
        }
        return retval;
    }


    public int getEventId(String name){
        //palauttaa ID-intin jos löytyy, muuten -1
        String source="http://www.finnkino.fi/xml/Events/";
        Document docu;
        URL finnkino;
        int id=-1;
        String buffer="", output="";
        System.out.println("Entering getEventId");
        try {
             finnkino = new URL (source);
             DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
             DocumentBuilder db = dbf.newDocumentBuilder();
             BufferedReader br = new BufferedReader(new InputStreamReader(finnkino.openStream()));
             while ((buffer=br.readLine())!=null)
                 output += buffer + "\n";
             docu = db.parse(new InputSource(new StringReader(output)));
             docu.getDocumentElement().normalize();
             //output = docu.getXmlVersion();
             //output="";
             NodeList nodes = docu.getElementsByTagName("Event");
             for (int i=0; i< nodes.getLength(); i++){
                 Element e = (Element)nodes.item(i);
                 String title = e.getElementsByTagName("Title").item(0).getTextContent();
                 if (title.equalsIgnoreCase(name))
                         id = new Integer(e.getElementsByTagName("ID").item(0).getTextContent());
                 }
             //outputField.setText(output);
        
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL");
        } catch (IOException ex){
            System.out.println("IOException");
        } catch (SAXException ex) {
            System.out.println("SAXException");
        } catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException");
        }
        System.out.println("Movie id was"+id);
        return id;
    }
    
    public ArrayList<Movie> getStartsBetween(ArrayList<Movie> input, String start, String end, String day){
        ArrayList<Movie> output = new ArrayList();
        Date timeStart=null, timeEnd=null;
        System.out.println("Entering getStartsBetween");
        try{
            timeStart = new SimpleDateFormat("dd.MM.yyyy.HH:mm").parse(day+"."+start);
            timeEnd = new SimpleDateFormat("dd.MM.yyyy.HH:mm").parse(day+"."+end);
        } catch (ParseException ex){
            System.out.println("Error parsing start or endtimes");
            System.out.println(day+"."+start);
        }
        
        for (Movie m : input)
            if (m.GetStartTime().before(timeEnd) && m.GetStartTime().after(timeStart))
                output.add(m);
        //remove all that don't fit criteria
        return output;
    }
    
    public ArrayList<Show> getEventShows(int eventId){
        ArrayList<Theatre> theatres = getTheatres();
        Document docu;
        URL finnkino;
        String output="", buffer;
        ArrayList<Show> retval = new ArrayList();
        System.out.println("Entering getEventShows loop");
        int count = 1;
        try {
            for (Theatre t : theatres){
                output=""; buffer="";
                System.out.println("checking theatre nr. " + count++);
                String source = "http://www.finnkino.fi/xml/Schedule/?area=" +t.getId();// "&dt="+ date;
                finnkino = new URL (source);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader(finnkino.openStream()));
                while ((buffer=br.readLine())!=null)
                    output += buffer + "\n";
                docu = db.parse(new InputSource(new StringReader(output)));
                docu.getDocumentElement().normalize();
                //output = docu.getXmlVersion();
                //output="";
                NodeList nodes = docu.getElementsByTagName("Show");
                for (int i=0; i< nodes.getLength(); i++){
                 Element e = (Element)nodes.item(i);
                 int eid = new Integer(e.getElementsByTagName("EventID").item(0).getTextContent());
                 if (eid==eventId){ //tässä on näytös, tallenetaan
                     System.out.println("found a show..");
                     String datestring = e.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                 
                    Date starts = null;
                    try{
                        starts = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(datestring);
                        } catch (ParseException ex){
                        System.out.println("Problem parsing starting time");
                        }
                 //int id = new Integer (e.getElementsByTagName("ID").item(0).getTextContent());
                 //retlist.add(new Theatre (id, name));
                    retval.add(new Show(new Theatre(t.getId(), t.getName()), starts));
                 }}
             //outputField.setText(output);
                }     
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL");
        } catch (IOException ex){
            System.out.println("IOException");
        } catch (SAXException ex) {
            System.out.println("SAXException");
        } catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException");
        }
        System.out.println("reached end, retval has " + retval.size() + " elements");
        return retval;
    }
    

}
