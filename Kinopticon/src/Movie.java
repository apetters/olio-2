
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Movie {
    private String name;
    private Date startTime;
    
    public Movie(String n, Date d){
        name = n;
        startTime = d;
    }
    
    public Date GetStartTime(){
        return startTime;
    }
    
    public String getName(){
        return name;
    }
    
    @Override
    public String toString(){
        return name;
    }
}
