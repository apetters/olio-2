
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Show {
    private Theatre place;
    private Date time;
    
    public Theatre getPlace(){
        return place;
    }
    
    public Date getTime(){
        return time;
    }
    
    public Show(Theatre t, Date d){
        place = t;
        time = d;
    }
    
    @Override
    public String toString(){
        return place.getName() + " : " + time;
    }
}
