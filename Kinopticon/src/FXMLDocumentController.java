/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author antepettersson
 */
public class FXMLDocumentController implements Initializable {
    //GregorianCalendar cal;
    
    private KinoXMLFeed dataSource;
    
    @FXML
    private ComboBox<Theatre> kinoComboBox;
    @FXML
    private TextArea outputField;
    @FXML
    private Button getMoviesButton;
    @FXML
    private TextField dateField;
    @FXML
    private ListView<Movie> movieListView;
    @FXML
    private TextField timeEnd;
    @FXML
    private TextField timeStart;
    @FXML
    private ListView<Show> showListView;
    @FXML
    private Button getShowsButton;
    @FXML
    private TextField titleTextField;
    @FXML
    private Label titleLabel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //cal = new GregorianCalendar();
        //cal.setTime(new Date());
        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        
        dateField.setText(sdf.format(now));
        dataSource = new KinoXMLFeed();
        ArrayList<Theatre> theatres = dataSource.getTheatres();
        //kinoComboBox.getItems().clear();
        for (Theatre t : theatres)
            kinoComboBox.getItems().add(t);
        // kinoComboBox.setValue(kinoComboBox.getItems().get(1));
    }    

    @FXML
    private void getMoviesButtonAction(ActionEvent event) {
        int id = kinoComboBox.getValue().getId();
        String date = dateField.getText();
        ArrayList<Movie> movies;
        String starts=timeStart.getText(), ends=timeEnd.getText();
        if (starts.equals("hh:mm") || ends.equals("hh:mm")){//ei asetettua aikaväliä{
            System.out.println("Ei asetettua aikaväliä");
            movies = dataSource.getMovies(id, date);
        }
        else{ //aikaväli asetettu
            System.out.println("starts: "+starts + " ends: "+ ends);
            movies = dataSource.getStartsBetween(dataSource.getMovies(id, date),starts,ends,date);
        }
        movieListView.getItems().clear();
        for (Movie m: movies)
            movieListView.getItems().add(m);
        //outputField.setText(movies);
    }

    @FXML
    private void getShowsButtonAction(ActionEvent event) {
        //titleLabel.setText("etsitään...");
        //titleLabel.setVisible(true);
        int id=dataSource.getEventId(titleTextField.getText());
        ArrayList<Show> shows = dataSource.getEventShows(id);
        showListView.getItems().clear();
        for (Show s : shows)
            showListView.getItems().add(s);
        if (shows.size()!=0){
            titleLabel.setText(titleTextField.getText() +":");
            titleLabel.setVisible(true);
        }else {
            titleLabel.setText("Ei näytöksiä");
            titleLabel.setVisible(true);
        }
        
    }

    @FXML
    private void kinoComboBoxSelectAction(ActionEvent event) {
        getMoviesButton.setDisable(false);
    }
    
}
