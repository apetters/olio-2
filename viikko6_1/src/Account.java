
import static java.lang.Math.round;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public abstract class Account {
    protected String number;
    protected double balance;
    
    Account(String number, double balance){
        this.number = number;
        this.balance = balance;
    }
    
    public String getAccountNumber() {return number;}
    
    public void deposit(double sum) {balance += sum;} 
    
    public abstract void withdraw(double sum);
    
    public void print(){
        System.out.println("Tilinumero: "+this.number+" Tilillä rahaa: "+round(this.balance));
    }
}

class NormalAccount extends Account {

    NormalAccount(String accountNumber, double balance) {
        super (accountNumber, balance);
        System.out.println("Tili luotu.");
    }
    public void withdraw(double sum){
        if (sum<=this.balance) this.balance-=sum;
        else System.out.println("Insufficient funds!");
    }
}

class CreditAccount extends Account {
    
    private double credit;
    
    CreditAccount(String accountNumber, double balance, double credit){
        super(accountNumber, balance);
        this.credit=credit;
        System.out.println("Tili luotu.");
    }
    public void withdraw(double sum){
        if (sum<this.balance+this.credit) this.balance-=sum;
        else System.out.println("Insufficient funds!");
    }
    
    public void print(){
        System.out.println("Tilinumero: "+this.number+" Tilillä rahaa: "+round(this.balance)+" Luottoraja: "+round(this.credit));
    }
 
}
