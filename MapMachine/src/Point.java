
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class Point {
    private String name;
    private Circle c;
    
    public Point(MouseEvent event){
        c = new Circle();
        c.setLayoutX(event.getX());
        c.setLayoutY(event.getY());
        c.setRadius(10);
        c.setFill(Color.BLUE);
        //c.setOnMouseClicked(FXMLDocumentController.mouseHandler(MouseEvent event));
        c.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Hei, olen piste! X: " + event.getSceneX() + " , Y: " + event.getSceneY());
                ShapeHandler sh = ShapeHandler.getInstance();
                if (sh.getChooseFirst()){ //first point
                    sh.lineStartX=event.getSceneX();
                    sh.lineStartY=event.getSceneY();
                    sh.setChooseFirst(false);
                    //erase old line if exists
                    
                } else { //second point
                    sh.lineEndX=event.getSceneX();
                    sh.lineEndY=event.getSceneY();
                    sh.setChooseFirst(true);
                    //draw a new line
                    System.out.println("piirretään viiva pisteestä " + sh.lineStartX + "," + sh.lineStartY + " pisteeseen "
                            + sh.lineEndX +"," +sh.lineEndY);
                    System.out.println(Parent.class);
                    
                    
                }
            }
        });
    }
    
    public String getName(){
        return name;
    }
    
    public Circle getCircle(){
        return c;
        
    }
    }
