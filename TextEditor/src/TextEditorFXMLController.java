/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

/**
 *
 * @author antepettersson
 */
public class TextEditorFXMLController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    @FXML
    private TextArea textArea;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void saveButtonAction(ActionEvent event) throws IOException {
        System.out.println("Tallennetaan");
        FileChooser file = new FileChooser();
        File savefile=file.showSaveDialog(null);
        FileIO writer=new FileIO(savefile);
        writer.writeFile(textArea.getText());
    }

    @FXML
    private void loadButtonAction(ActionEvent event) throws IOException {
        System.out.println("Ladataan");
        FileChooser file = new FileChooser();
        File loadfile=file.showOpenDialog(null);
        FileIO reader=new FileIO(loadfile);
        textArea.setText(reader.readFile());
    }
    
}
