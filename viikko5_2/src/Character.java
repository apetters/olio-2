/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public abstract class Character {
    
    public WeaponBehavior weapon;
    
    public Character () {}
    
    abstract public void fight();
}
    
class Axe extends WeaponBehavior {

  public Axe () { };
  public void useWeapon () { };
}



class Club extends WeaponBehavior {
  public Club () { };
  public void useWeapon(  )
  {
  }


}

class Knife extends WeaponBehavior {
    public Knife() {};
    public void useWeapon() {};
}

class King extends Character {
  public King () { };
  public void fight(  )
  {
  }


}


class Knight extends Character {
  public Knight () { };
  public void fight(  )
  {
  }


}
class Queen extends Character {
  public Queen () { };
  public void fight(  )
  {
  }


}


class Sword extends WeaponBehavior {
  public Sword () { };
  public void useWeapon(  )
  {
  }


}

class Troll extends Character {
public Troll () { };
  public void fight(  )
  {
  }


}

abstract class WeaponBehavior {
  public WeaponBehavior () { };
  abstract public void useWeapon(  );


}