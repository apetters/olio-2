
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean quit=false;
        Character character=null;
        int choice;
        Scanner choose=new Scanner(System.in);
        while (!quit){
            System.out.print("*** TAISTELUSIMULAATTORI ***\n"
                    + "1) Luo hahmo\n"
                    + "2) Taistele hahmolla\n"
                    + "0) Lopeta\n"
                    + "Valintasi: ");
        choice=choose.nextInt();
        switch (choice){
            case 1:
                System.out.print("Valitse hahmosi:\n"
                        + "1) Kuningas\n"
                        + "2) Ritari\n"
                        + "3) Kuningatar\n"
                        + "4) Peikko\n"
                        + "Valintasi: ");
                choice=choose.nextInt();
                switch (choice){
                    case 1:
                        character=new King();
                        break;
                    case 2:
                        character=new Knight();
                        break;
                    case 3:
                        character=new Queen();
                        break;
                    case 4:
                        character=new Troll();
                        break;
                }
                System.out.print("Valitse aseesi: \n" +
"1) Veitsi\n" +
"2) Kirves\n" +
"3) Miekka\n" +
"4) Nuija");
                choice=choose.nextInt();
                switch (choice){
                    case 1:
                        character.weapon=new Knife();
                        break;
                    case 2:
                        character.weapon=new Axe();
                        break;
                    case 3:
                        character.weapon=new Sword();
                        break;
                    case 4:
                        character.weapon=new Club();
                        break;
                }
                break;
                
            case 2:
                System.out.println(character.getClass().getSimpleName() + " taistelee aseella "+character.weapon.getClass().getSimpleName());
                break;
                
            case 0:
                quit = true;
                break;
                
        }
    }
}
}