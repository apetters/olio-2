/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private float total_energy;
    private float price;
    private float size;
    
    public Bottle(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3f;
        size = 0.5f;
        price = 1.80f;
    }
    
    public Bottle(String n, String m, float e, float s, float p){
        name = n;
        manufacturer = m;
        total_energy = e;
        size = s;
        price = p;
    }
    
    public String getName(){
        return name;
    }
    
    public float getPrice(){
        return price;
    }
    
    public float getSize(){
        return size;
    }
    
    @Override
    
    public String toString(){
        return name + " "+ size + "l";
    }
    
}

